FROM cypress/browsers:chrome69
RUN mkdir /code
ADD . /code
WORKDIR /code
RUN npm i cypress
ENTRYPOINT $(npm bin)/cypress run --browser chrome
