describe('Silbia Web Login', function() {
    it('.should() - assert that <title> is correct', function(){
        cy.visit(Cypress.env('web_site'));
        cy.title().should('include', Cypress.env('title_login'));
    });
});


describe('Login Silbia', function() {
    it('si el usuario existe la url debe ser', function() {
       cy.viewport(1440, 821)
       cy.clearCookies()
       cy.visit(Cypress.env('web_site'))
       cy.get('#body_login > #new_user > .form-inputs > .form-group > #user_email').type(Cypress.env('user_login'))
       cy.get('#body_login > #new_user > .form-inputs > .form-group > #user_password').type(Cypress.env('password_login'))
       cy.get('.card > #body_login > #new_user > .form-actions > #btn_login').click()
       cy.url().should('equal',Cypress.env ('url_home'))
    });

    it('si el usuario es solo tipo editor', function(){
        cy.get('.cargo').should('contain', 'EDITOR');
    });

    it('cantidad de silabus cargados mayor a 0', function(){
        cy.get('.badge').not('contain', '0');
    });

    describe('Evaluar tabla segun tipo de usuario', function() {
        it('si el usuario es editor, deberian mostrarse 3 columnas (estado, curso y programa)', function(){
            if (cy.get('.cargo').should('contain', 'EDITOR')) {
                cy.get('thead > tr > :nth-child(1)').should('contain', 'Estado');
                cy.get('thead > tr > :nth-child(2)').should('contain', 'Curso');
                cy.get('thead > tr > :nth-child(3)').should('contain', 'Programa');
            }
        });
    });

    describe('Test de silabus', function() {
        it('si el estado es pendiente, edición y observacion, el boton deberìa ser "Editar"', function(){
            cy.get('.table > tbody > tr').each(function(element) {
                var span = element[0].children[0].children[0].innerHTML
                if (span === ' EN EDICIÓN ') {
                    expect(element[0].children[3].children[0].innerHTML).equal('Editar');
                } else if (span === ' PENDIENTE ') {
                    expect(element[0].children[3].children[0].innerHTML).equal('Editar');
                } else if (span === ' EN OBSERVACIÓN ') {
                    expect(element[0].children[3].children[0].innerHTML).equal('Editar');
                }
            });
        });
    });

    describe('Ingresar al curso indicado hardcode', function(){
        it('ingresar al curso de pregrado, segundo en la lista', function() {
            cy.visit(Cypress.env('web_site'))
            cy.get('#body_login > #new_user > .form-inputs > .form-group > #user_email').type(Cypress.env('user_login'))
            cy.get('#body_login > #new_user > .form-inputs > .form-group > #user_password').type(Cypress.env('password_login'))
            cy.get('.card > #body_login > #new_user > .form-actions > #btn_login').click()
            cy.get(':nth-child(1) > :nth-child(4) > .btn').click()
          
            
        })
    })

    describe('Validar mostrar modal', function(){
        it ('carga el formulario de registro del sílabo y aceptar modal', function(){
            cy.get ('.modal-mejora > .btn__content > .btn').click()
            
            })
 
        })

    })





